const express = require('express');
const cookieParser = require('cookie-parser');
const jokeController = require('./controllers/jokeController');
const path = require('path');

const app = express();
app.use(cookieParser());
app.use(express.urlencoded({ extended: true }))

// Serve static files
app.use('/static', express.static(path.join(__dirname, 'static')));

// Set the view engine to use EJS
app.set('view engine', 'ejs');

// Set up the route to handle requests for jokes
app.get('/', jokeController.getRandomJoke);

// Set up the route to handle votes
app.post('/vote', jokeController.vote);

// Start the server
app.listen(3000, () => {
    console.log('Server started on port 3000');
});