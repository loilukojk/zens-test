const { jokes, votes } = require('../models/joke');
const path = require('path');

function hasUserVoted(cookie, jokeId) {
    if (!votes[cookie]) {
        return false;
    }
    return votes[cookie].includes(jokeId);
}

function getRandomJoke(req, res) {
    const cookie = req.cookies.userVote;

    // Get a random unseen joke
    const unseenJokes = jokes.filter(joke => !hasUserVoted(cookie, joke.id));
    if (unseenJokes.length === 0) {
        res.sendFile(path.join(__dirname + '/../views/no-more-jokes.html'));
        return;
    }

    const randomIndex = Math.floor(Math.random() * unseenJokes.length);
    const joke = unseenJokes[randomIndex];
    res.render(__dirname + '/../views/index', { joke });
}

function vote(req, res) {
    const jokeId = parseInt(req.body.jokeId);
    const vote = req.body.vote;
    console.log(`user voted ${vote} for joke with id ${jokeId}`)

    const cookie = req.cookies.userVote || Math.random().toString();
    if (!votes[cookie]) {
        votes[cookie] = [];
    }
    votes[cookie].push(jokeId);

    res.cookie('userVote', cookie);

    res.redirect('/');
}

module.exports = {
    hasUserVoted,
    getRandomJoke,
    vote,
};