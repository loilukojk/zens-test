export const testcases = [
    {
        id: 1,
        title: 'positive numbers',
        input: [5, 10, 2, 8, 3],
        output: 10
    },
    {
        id: 2,
        title: 'negative numbers',
        input: [-5, -10, -2, -8, -3],
        output: -2
    },
    {
        id: 3,
        title: 'negative & positive numbers',
        input: [-5, -10, 2, -8, 3],
        output: 3
    },
    {
        id: 4,
        title: '2 equal numbers',
        input: [5, 10, 5, 2, 3],
        output: 10
    },
    {
        id: 5,
        title: 'there is a number lower than 32bit negative integer',
        input: [5, 10, 5, 5, -2147483649],
        output: 10
    },
    {
        id: 6,
        title: 'there is a number greater than 32bit positive integer',
        input: [1, 10, 5, 5, 2147483648],
        output: 2147483648
    }
]