export const testcases = [
    {
        id: 1,
        title: 'positive numbers',
        input: [5, 10, 2, 8, 3],
        output: [18, 26]
    },
    {
        id: 2,
        title: 'negative numbers',
        input: [-5, -10, -2, -8, -3],
        output: [-26, -18]
    },
    {
        id: 3,
        title: 'negative & positive numbers',
        input: [-5, -10, 2, -8, 3],
        output: [-21, -8]
    },
    {
        id: 4,
        title: '2 equal numbers',
        input: [5, 10, 5, 2, 3],
        output: [15, 23]
    },
    {
        id: 5,
        title: 'sum is a number greater than 32bit integer',
        input: [5, 10, 5, 5, 2147483647],
        output: [25, 2147483667]
    }
]