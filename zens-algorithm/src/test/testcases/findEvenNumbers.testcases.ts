export const testcases = [
    {
        id: 1,
        title: 'positive numbers',
        input: [5, 10, 2, 8, 3],
        output: [10, 2, 8]
    },
    {
        id: 2,
        title: 'negative numbers',
        input: [-5, -10, -2, -8, -3],
        output: [-10, -2, -8]
    },
    {
        id: 3,
        title: 'negative & positive numbers',
        input: [-5, -10, 2, -8, 3],
        output: [-10, 2, -8]
    },
    {
        id: 4,
        title: 'there is a number is zero',
        input: [5, 10, 5, 0, 3],
        output: [10, 0]
    },
    {
        id: 5,
        title: 'have no even number',
        input: [5, 7, 5, 5, 2147483647],
        output: []
    },
    {
        id: 6,
        title: 'there is a number greater than 32bit positive number',
        input: [5, 7, 5, 5, 2147483648],
        output: [2147483648]
    },
    {
        id: 7,
        title: 'there is a number lower than 32bit negative number',
        input: [5, 7, 2, 5, -2147483649],
        output: [2]
    }
]