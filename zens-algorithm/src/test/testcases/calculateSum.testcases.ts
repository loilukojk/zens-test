export const testcases = [
    {
        id: 1,
        title: 'positive numbers',
        input: [5, 10, 2, 8, 3],
        output: 28
    },
    {
        id: 2,
        title: 'negative numbers',
        input: [-5, -10, -2, -8, -3],
        output: -28
    },
    {
        id: 3,
        title: 'negative & positive numbers',
        input: [-5, -10, 2, -8, 3],
        output: -18
    },
    {
        id: 4,
        title: 'sum is a number greater than 32bit integer',
        input: [5, 10, 5, 5, 2147483647],
        output: 2147483672
    }
]