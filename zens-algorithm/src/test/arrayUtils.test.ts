import {
    miniMaxSum,
    calculateSum,
    findSmallestNumber,
    findLargestNumber,
    findEvenNumbers,
    findOddNumbers
} from '../arrayUtils';

import * as miniMaxSumTC from './testcases/miniMaxSum.testcases';
import * as calculateSumTC from './testcases/calculateSum.testcases';
import * as findSmallestNumberTC from './testcases/findSmallestNumber.testcases';
import * as findLargestNumberTC from './testcases/findLargestNumber.testcases';
import * as findEvenNumbersTC from './testcases/findEvenNumbers.testcases';
import * as findOddNumbersTC from './testcases/findOddNumbers.testcases';

describe('miniMaxSum', () => {
    it.each(miniMaxSumTC.testcases)('miniMaxSum testcase $id: $title', ({ input, output }) => {
        expect(miniMaxSum(input)).toEqual(output);
    })
});

describe('calculateSum', () => {
    it.each(calculateSumTC.testcases)('calculateSum testcase $id: $title', ({ input, output }) => {
        expect(calculateSum(input)).toEqual(output);
    })
});

describe('findSmallestNumber', () => {
    it.each(findSmallestNumberTC.testcases)('findSmallestNumber testcase $id: $title', ({ input, output }) => {
        expect(findSmallestNumber(input)).toEqual(output);
    })
});

describe('findLargestNumber', () => {
    it.each(findLargestNumberTC.testcases)('findLargestNumber testcase $id: $title', ({ input, output }) => {
        expect(findLargestNumber(input)).toEqual(output);
    })
});

describe('findEvenNumbers', () => {
    it.each(findEvenNumbersTC.testcases)('findEvenNumbers testcase $id: $title', ({ input, output }) => {
        expect(findEvenNumbers(input)).toEqual(output);
    })
});

describe('findOddNumbers', () => {
    it.each(findOddNumbersTC.testcases)('findOddNumbers testcase $id: $title', ({ input, output }) => {
        expect(findOddNumbers(input)).toEqual(output);
    })
});
