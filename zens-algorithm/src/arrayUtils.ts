// Feat: Find minimum sum & maximum sum
export function miniMaxSum(arr: number[]): number[] {
    arr.sort((a, b) => a - b);
    let sum = arr.reduce((sum, num) => sum + num, 0);
    let minSum = sum-arr[arr.length-1]
    let maxSum = sum-arr[0]
    return [minSum, maxSum]
}

// Feat: Find sum
export function calculateSum(arr: number[]): number {
    return arr.reduce((sum, num) => sum + num, 0);
}

// Feat: Find smallest number
export function findSmallestNumber(arr: number[]): number {
    return Math.min(...arr);
}

// Feat: Find largest number
export function findLargestNumber(arr: number[]): number {
    return Math.max(...arr);
}

// Feat: Find even numbers
export function findEvenNumbers(arr: number[]): number[] {
    return arr.filter(num => num % 2 === 0);
}

// Feat: Find odd numbers
export function findOddNumbers(arr: number[]): number[] {
    return arr.filter(num => num % 2 !== 0);
}
