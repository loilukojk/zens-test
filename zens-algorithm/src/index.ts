import * as readline from 'readline';
import {
    miniMaxSum,
    calculateSum,
    findSmallestNumber,
    findLargestNumber,
    findEvenNumbers,
    findOddNumbers
} from './arrayUtils';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

function processInput() {
    rl.question('Nhập 1 để sử dụng các hàm tính toán hoặc 2 để dừng chương trình: ', (choice: string) => {
        if (choice === '1') {
            rl.question('Nhập chuỗi 5 số, cách nhau bởi dấu cách: ', (input: string) => {
                const numbers: number[] = input.split(' ').map(Number);
                if (numbers.length !== 5 || numbers.some(isNaN)) {
                    console.log('Định dạng input bạn vừa nhập không hỗ trợ. Vui lòng nhập đúng 5 số nếu muốn trải nghiệm.');
                    processInput();
                    return;
                }

                const [minSum, maxSum] = miniMaxSum(numbers);
                console.log('Kết quả (minSum, maxSum):', minSum, maxSum);

                const sum = calculateSum(numbers);
                console.log('Tổng của mảng:', sum);

                const smallestNumber = findSmallestNumber(numbers);
                console.log('Số nhỏ nhất trong mảng:', smallestNumber);

                const largestNumber = findLargestNumber(numbers);
                console.log('Số lớn nhất trong mảng:', largestNumber);

                const evenNumbers = findEvenNumbers(numbers);
                console.log('Các số chẵn trong mảng:', evenNumbers);

                const oddNumbers = findOddNumbers(numbers);
                console.log('Các số lẻ trong mảng:', oddNumbers);

                processInput();
            });
        } else if (choice === '2') {
            rl.close();
        } else {
            console.log('Lựa chọn không hợp lệ. Vui lòng chọn lại.');
            processInput();
        }
    });
}

processInput();